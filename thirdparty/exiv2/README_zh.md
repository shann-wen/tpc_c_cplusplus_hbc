# exiv2三方库说明
## 功能简介
exiv2是一个C++库和命令行实用程序，用于读取、写入、删除和修改Exif、IPTC、XMP和ICC图像元数据。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v0.27.6
- 当前适配的功能：读取、写入、删除和修改Exif、IPTC、XMP和ICC图像元数据
- [GPL2.0](https://github.com/Exiv2/exiv2)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
