# FreeType三方库说明
## 功能简介
FreeType是用C语言编写的。它设计为小巧、高效且高度可定制，同时能够为数字排版生成大多数矢量和位图字体格式的高质量输出（字形图像）。FreeType是一个免费提供的便携式软件库，用于渲染字体
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：2.13.0
- 当前适配的功能：支持数字排版生成大多数矢量和位图字体格式，免费提供的便携式软件库，用于渲染字体。
- [ General Public License](https://github.com/freetype/freetype/blob/master/LICENSE.TXT)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
