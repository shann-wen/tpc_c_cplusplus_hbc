# websocketpp三方库说明
## 功能简介
websocketpp C++ websocket client/server library。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：0.8.2
- 当前适配的功能：websocket 客户端，服务端
- [ Peter Thorson. All rights reserved ](https://github.com/zaphoyd/websocketpp/blob/master/COPYING)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
