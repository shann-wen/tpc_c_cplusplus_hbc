# zlib三方库说明
## 功能简介
zlib是提供数据压缩用的函式库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v1.2.13
- 当前适配的功能：支持数据压缩能力
- [zlib License](https://github.com/madler/zlib/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
