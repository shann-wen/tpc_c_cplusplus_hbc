# lcms2三方库说明
## 功能简介
Little cms is a color management library. Implements fast transforms between ICC profiles. It is focused on speed, and is portable across several platforms.
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：2.15 
- 当前适配的功能：颜色管理
- [ MIT license](https://sourceforge.net/projects/lcms)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
