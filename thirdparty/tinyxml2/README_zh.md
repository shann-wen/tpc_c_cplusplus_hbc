# tinyxml2三方库说明
## 功能简介
TinyXML2 是 simple、small、efficient 的开源 C++ XML 文件解析库，可以很方便地应用到现有的项目中。
## 使用约束
- ROM版本：OpenHarmony-v3.2-Beta1
- IDE版本：DevEco Studio 3.1 Beta1(3.1.0.200)
- SDK：API9 Release(3.2.10.6)
- 三方库版本：v9.0.0

- 当前适配的功能：xml解析

- [zlib License](https://github.com/leethomason/tinyxml2/blob/9.0.0/LICENSE.txt)
## 集成方式
+ [系统Rom包集成](docs/rom_integrate.md)
+ [应用Hap包集成](docs/hap_integrate.md)
