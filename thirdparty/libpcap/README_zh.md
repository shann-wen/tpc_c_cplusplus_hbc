# libpcap三方库说明
## 功能简介
libpcap是unix/linux平台下的网络数据包捕获函数包，大多数网络监控软件都以它为基础。

## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：libpcap-1.10.3
- 当前适配的功能：支持网络数据包捕获
- [BSD](https://github.com/the-tcpdump-group/libpcap/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
