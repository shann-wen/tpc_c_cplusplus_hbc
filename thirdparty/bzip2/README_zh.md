# bzip2三方库说明
## 功能简介
bzip2 是使用 Burrows–Wheeler 算法，压缩解压文件。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：1.0.6
- 当前适配的功能：压缩解压文件
- [Bzip2 License](https://sourceforge.net/p/bzip2/bzip2/ci/master/tree/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
