# libjpeg-turbo 三方库说明

## 功能简介

libjpeg-turbo是一种JPEG图像编解码器，它使用SIMD指令来加速基准JPEG压缩和解压缩.

## 使用约束

- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：2.1.91
- 当前适配的功能：支持JPEG图像编解码。
- [IJG(Independent JPEG Group) License/3-clause BSD License/zlib License](https://github.com/libjpeg-turbo/libjpeg-turbo/blob/main/LICENSE.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
