# unixODBC三方库说明
## 功能简介
unixODBC项目的目标是开发和推广unixODBC，使其成为非MS Windows平台上ODBC的最终标准。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：2.3.11
- 当前适配的功能：unixODBC项目的目标是开发和推广unixODBC，使其成为非MS Windows平台上ODBC的最终标准。
- [GNU Lesser General Public License v2.1](https://github.com/lurcher/unixODBC/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
