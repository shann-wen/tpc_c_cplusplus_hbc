# nghttp2三方库说明
## 功能简介
nghttp2是一个实现http2超文本传输协议的C库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v1.52.0
- 当前适配的功能：支持http2协议
- [MIT](https://github.com/nghttp2)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
