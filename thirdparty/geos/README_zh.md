# geos三方库说明
## 功能简介
GEOS是一个C++库，用于对二维矢量几何图形执行操作。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：3.11.2
- 当前适配的功能：处理二维矢量几何图形
- [LGPL v2.1](https://github.com/libgeos/geos)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
