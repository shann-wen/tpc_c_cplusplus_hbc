# marisa 三方库说明

## 功能简介

marisa 递归存储匹配算法种静态的、节省空间的trie数据结构.

## 使用约束

- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：4.0.0
- 当前适配的功能：提供字典树数据结构算法，用于生成并加载asr字典树.
- [LGPL-2.1/GPL-2.0](https://github.com/s-yata/marisa-trie/blob/master/COPYING.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
