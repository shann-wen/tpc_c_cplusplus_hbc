# lame三方库说明
## 功能简介
lame是非常优秀的一种MP3编码器。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：3.100
- 当前适配的功能：MP3编码器
- [LGPLv2/GPLv2 license](https://sourceforge.net/projects/lame/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
