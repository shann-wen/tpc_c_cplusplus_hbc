# assimp三方库说明
## 功能简介
assimp作为一个开源项目，设计了一套可扩展的架构，为模型的导入导出提供了良好的支持。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：5.2.5
- 当前适配的功能：支持导入导出3D模型数据
- [3-clause BSD](https://github.com/assimp/assimp/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
