# p7zip三方库说明
## 功能简介
p7zip 是一个功能齐全的压缩打包应用。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v17.05 
- 当前适配的功能：文件压缩，解压，打包，解包
- [ LGPL license](https://github.com/p7zip-project/p7zip)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
