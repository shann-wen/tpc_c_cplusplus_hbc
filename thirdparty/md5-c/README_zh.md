# md5-c三方库说明
## 功能简介
  md5的全称是md5信息摘要算法，用于确保信息传输的完整一致。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：main
- 当前适配的功能：提供数据的序列化框架
- [BSD-3-Clause license](https://github.com/Zunawe/md5-c/blob/main/UNLICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)