# uchardet 三方库说明
## 功能简介
uchardet is an encoding detector library, which takes a sequence of bytes in an unknown character encoding without any additional information, and attempts to determine the encoding of the text. Returned encoding names are iconv-compatible.
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v0.0.8
- 当前适配的功能：encoding detector
- [ "MOZILLA PUBLIC LICENSE" "GPLV2" "LGPLV2" ](https://www.freedesktop.org/wiki/Software/uchardet/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
