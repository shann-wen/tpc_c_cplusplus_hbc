# soundtouch三方库说明
## 功能简介
soundtouch是一个提供音频变速变调能力的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：2.3.2
- 当前适配的功能：音频变速变调能力
- [LGPLv2.1](https://www.surina.net/soundtouch/download.html)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
