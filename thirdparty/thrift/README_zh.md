# thrift三方库说明
## 功能简介
Thrift是一种接口描述语言和二进制通讯协议，它被用来定义和创建跨语言的服务。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v0.18.1
- 当前适配的功能：支持跨语言服务创建、数据传输和远程过程调用的高性能二进制协议
- [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
