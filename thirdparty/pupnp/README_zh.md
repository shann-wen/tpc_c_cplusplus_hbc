# pupnp三方库说明
## 功能简介
pupnp是提供UPnP协议能力的三方库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v1.0.12
- 当前适配的功能：支持Upnp协议能力
- [LGPL-2.1 license](https://github.com/pupnp/pupnp/blob/branch-1.14.x/COPYING)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
