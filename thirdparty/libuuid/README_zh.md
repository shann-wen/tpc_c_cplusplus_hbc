# libuuid三方库说明
## 功能简介
The libuuid library is used to generate unique identifiers for objects that may be accessible beyond the local system. 
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：1.0.3 
- 当前适配的功能：生成唯一识别码
- [ BSD license](https://sourceforge.net/projects/libuuid/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
