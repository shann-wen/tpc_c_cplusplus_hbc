# libxls三方库说明
## 功能简介
libxls是一个解析Excel表格的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v1.6.2
- 当前适配的功能：解析Excel表格
- [BSD](https://github.com/libxls/libxls)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
