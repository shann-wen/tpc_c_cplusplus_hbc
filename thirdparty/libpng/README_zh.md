# libpng三方库说明
## 功能简介
libpng是一款C语言编写的用来读写PNG文件的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：1.6.39
- 当前适配的功能：处理PNG文件的读写
- [zlib/libpng License](https://sourceforge.net/projects/libpng/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
