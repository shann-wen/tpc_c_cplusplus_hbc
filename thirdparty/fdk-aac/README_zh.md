# fdk-aac三方库说明
## 功能简介
fdk-aac是一个开源的AAC编码库，被认为是开源AAC编码器中音质最好的之一。它支持多种编码模式，包括LC-AAC、HE-AAC和HE-AAC V2
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v2.0.2
- 当前适配的功能：开源的AAC编码库、支持多种编码模式，包括LC-AAC、HE-AAC和HE-AAC V2
- [Apache License 2.0](https://github.com/mstorsjo/fdk-aac/blob/master/METADATA)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
