# giflib三方库说明

## 功能简介

giflib是一个用于阅读和编写gif图像的库

## 使用约束

- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：5.2.1
- 当前适配的功能：阅读和编写gif图像
- [giflib COPYING](https://sourceforge.net/p/giflib/code/ci/master/tree/COPYING)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
