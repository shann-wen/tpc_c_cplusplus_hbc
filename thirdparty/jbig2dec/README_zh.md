# jbig2dec三方库说明
## 功能简介
jbig2dec是JBIG2图像压缩格式的解码器实现。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：0.19
- 当前适配的功能：JBIG2图像压缩格式的解码
- [AGPLv3](https://jbig2dec.com/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
