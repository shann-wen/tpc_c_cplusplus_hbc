# zbar三方库说明
## 功能简介
zbar是一个条形码和二维码解析的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：0.10
- 当前适配的功能：支持条形码和二维码解析
- [LGPL2.1](https://zbar.sourceforge.net/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
