# stb三方库说明
## 功能简介
stb是一个图像读写库
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：master
- 当前适配的功能：图像加载、写出、缩放等功能
- [MIT License](https://github.com/nothings/stb/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)