# libsrtp三方库说明
## 功能简介
libSRTP提供了保护RTP和RTCP的功能。RTP数据包可以进行加密和身份验证（使用srtp_protect（）函数），将其转换为srtp数据包。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v2.5.0 
- 当前适配的功能：支持加密的rtp数据包传输
- [2001-2017 Cisco Systems, Inc](https://github.com/cisco/libsrtp/blob/main/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
