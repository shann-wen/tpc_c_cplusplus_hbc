# tinyexr三方库说明
## 功能简介
tinyexr是加载和保存OpenEXR(.exr) 映像的小型库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v1.0.2
- 当前适配的功能：支持加载和保存OpenEXR的能力
- [3-clause BSD/public domain](https://github.com/syoyo/tinyexr#readme)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
