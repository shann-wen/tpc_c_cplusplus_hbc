# Leptonica 三方库说明

## 功能简介

Leptonica是一个开放源码的C语言库，它被广泛地运用于图像处理和图像分析。

## 使用约束

- ROM版本：OpenHarmony3.2 Beta1
- IDE版本：DevEco Studio 3.1 Release(3.1.0.500)
- SDK：API9 Release(3.2.12.2)
- 三方库版本：1.83.1
- 当前适配的功能：已验证扫描的图片“去污”、“提色”功能和图片转pdf功能等
- [License : leptonica-license](http://www.leptonica.org/source/README.html)

## 集成方式

- [系统hap包集成](docs/hap_integrate.md)
- [系统Rom包集成](docs/rom_integrate.md)
