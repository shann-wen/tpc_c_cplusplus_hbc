# tcl三方库说明
## 功能简介
tcl是一种解释语言，也是该语言的一种非常便携的解释器。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：8.6.13
- 当前适配的功能：支持tcl解释器
- [BSD](https://sourceforge.net/projects/tcl/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
