# libexpat三方库说明
## 功能简介
libexpat是一个用于解析XML 1.0的C99库，面向流的XML解析器。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：R_2_5_0
- 当前适配的功能：解析XML
- [MIT](https://github.com/libexpat/libexpat)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
