# phf三方库说明
## 功能简介
phf是实现完美hash算法的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：rel-20190215
- 当前适配的功能：支持完美hash算法
- [MIT license](https://github.com/wahern/phf/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
