# libtess2三方库说明
## 功能简介
libtess2 可以对复杂多边形进行曲面细分。
## 使用约束
- IDE版本：DevEco Studio 3.1 Release
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：1.3.1
- 当前适配的功能：支持对复杂多边形进行曲面细分
- [SGI FREE SOFTWARE LICENSE B](https://github.com/memononen/libtess2/blob/master/LICENSE.txt)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
