# libsvm三方库说明
## 功能简介
libsvm是一个支持向量机的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v331
- 当前适配的功能：支持向量机
- [BSD](https://github.com/cjlin1/libsvm)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
