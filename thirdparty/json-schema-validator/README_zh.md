# json-schema-validator三方库说明
## 功能简介
json-schema-validator用于验证基于JSON Schema的JSON文档。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：2.2.0
- 当前适配的功能：验证JSON Schema的JSON文档
- [MIT](https://github.com/pboettch/json-schema-validator)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
