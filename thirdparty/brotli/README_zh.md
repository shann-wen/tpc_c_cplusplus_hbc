# brotli三方库说明
## 功能简介
Brotli is a generic-purpose lossless compression algorithm that compresses data using a combination of a modern variant of the LZ77 algorithm, Huffman coding and 2nd order context modeling, with a compression ratio comparable to the best currently available general-purpose compression methods. It is similar in speed with deflate but offers more dense compression
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：2.13.0
- 当前适配的功能：支持无损压缩算法。
- [MIT License](https://github.com/google/brotli/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
