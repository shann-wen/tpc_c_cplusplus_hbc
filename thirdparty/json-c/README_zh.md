# json-c三方库说明
## 功能简介
json-c是json数据解析库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：0.16-20220414
- 当前适配的功能：支持json数据解析
- [MIT license](https://github.com/json-c/json-c/blob/master/COPYING)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
