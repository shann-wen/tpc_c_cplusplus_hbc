# zxing-cpp三方库说明
## 功能简介
zxing-cpp是一个二维码生成和解析的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v2.0.0
- 当前适配的功能：二维码生成和解析
- [Apache License 2.0](https://github.com/zxing-cpp/zxing-cpp)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
