# tinyxpath三方库说明
## 功能简介
tinyxpath用于从 XML 树中提取 XPath 1.0 表达式。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：1.3.1
- 当前适配的功能：支持xpath语法解析
- [zlib/libpng License](https://sourceforge.net/projects/tinyxpath/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
