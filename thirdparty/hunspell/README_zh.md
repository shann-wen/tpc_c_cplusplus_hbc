# hunspell 三方库说明

## 功能简介

hunspell 是一个免费的拼写检查器和形态分析器库和命令行工具.

## 使用约束

- IDE版本：DevEco Studio 3.1 Release
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v1.7.2
- 当前适配的功能：支持拼写检查以及形态分析。
- [LGPL-2.1/GPL-2.0 License](https://github.com/hunspell/hunspell/blob/master/COPYING.LESSER)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
