# sqliteodbc三方库说明
## 功能简介
sqliteodbc是基于SQLite数据库的ODBC驱动程序
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：0.9998
- 当前适配的功能：基于SQLite3数据库的ODBC驱动程序。
- [BSD](http://www.ch-werner.de/sqliteodbc/license.terms)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
