# libarchive 三方库说明
## 功能简介
libarchive项目开发了一种便携式，高效的C库，可以以各种格式读取和编写流库。它还包括使用libarchive库的常见功能包括，tar,cpio 和 zcat 命令行工具的实现。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 4.0.3.6 (API Version 9 Release)
- 三方库版本：v3.6.2
- libarchive项目开发了一种便携式，高效的C库，可以以各种格式读取和编写流库
- [GNU Library General Public License](https://github.com/libarchive/libarchive/blob/master/COPYING)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
