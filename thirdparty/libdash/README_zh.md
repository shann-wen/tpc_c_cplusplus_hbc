# libdash三方库说明
## 功能简介
libdash是ISO/IEC MPEG-DASH标准的官方参考软件，为Bitmovin开发的MPEG-DASH提供面向对象（OO）接口。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v2.2
- 当前适配的功能：支持MPEG-DASH编码
- [LGPLv2.1](https://github.com/bitmovin/libdash)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
