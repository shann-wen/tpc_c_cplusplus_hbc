# nghttp3三方库说明
## 功能简介
nghttp3是在C中通过QUIC和QPACK进行HTTP/3映射的实现，它不依赖于任何特定的QUIC传输实现
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v0.10.0
- 当前适配的功能：支持http3
- [MIT License](https://github.com/ngtcp2/nghttp3/blob/main/COPYING)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
