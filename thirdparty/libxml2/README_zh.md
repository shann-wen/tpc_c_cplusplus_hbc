# libxml2三方库说明
## 功能简介
libxml2是一个用于像素操作的低级软件库，提供图像合成和梯形光栅化等功能。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：0.42.2 
- 当前适配的功能：像素操作，图像合成，光栅化
- [ MIT license](https://github.com/GNOME/libxml2)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
