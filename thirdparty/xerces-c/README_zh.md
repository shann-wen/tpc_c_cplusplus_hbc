# xerces-c三方库说明
## 功能简介
xerces是一个开放源代码的XML语法分析器，它提供了SAX和DOM API。
## 使用约束
- IDE版本：DevEco Studio 3.1 Release(3.1.0.500)
- SDK：API9 Release(3.2.12.2)
- 三方库版本：v3.2.4
- 当前适配的功能：支持sax解析和dom解析
- [Apache License 2.0](https://github.com/apache/xerces-c/blob/master/LICENSE)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
+ [系统Rom包集成](docs/rom_integrate.md)
