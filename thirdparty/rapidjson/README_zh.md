# rapidjson三方库说明
## 功能简介
RapidJson是一个跨平台的c++的json的解析器和生成器。
## 使用约束
- ROM版本：OpenHarmony-v3.2-Beta1
- 三方库版本：v1.1.0
- 当前适配的功能：支持json数据的解析和生成。
- [MIT license](https://github.com/Tencent/rapidjson/blob/master/license.txt)
## 集成方式
+ [系统Rom包集成](docs/rom_integrate.md)
