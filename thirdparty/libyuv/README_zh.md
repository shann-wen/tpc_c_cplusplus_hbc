# libyuv三方库说明
## 功能简介
libyuv是一个开源的图像处理库，提供了多种图像处理功能，包括图像格式转换、颜色空间转换、颜色调整、去噪、去雾、锐化、缩放等。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：c0031cf(该版本为镜像仓版本，官方版本：c0031cfd95e131c7b11be41d0272455cc63f10f4)
- 当前适配的功能：支持多种图像处理功能，包括图像格式转换、颜色空间转换、颜色调整、去噪、去雾、锐化、缩放等
- BSD-3-Clause license (地址见README.OpenSource文件)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
