# faad2三方库说明
## 功能简介
faad2是一个音频解码的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：2.10.1
- 当前适配的功能：支持音频的解码
- [GPLv2](https://github.com/knik0/faad2)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
