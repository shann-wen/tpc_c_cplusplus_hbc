# libheif三方库说明
## 功能简介
libheif是HEIF和AVIF文件格式编解码三方库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v1.15.2
- 当前适配的功能：支持HEIF和AVIF文件格式编解码
- [GNU LESSER GENERAL PUBLIC LICENSE](https://github.com/strukturag/libheif/blob/master/COPYING)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
