# jpeg三方库说明
## 功能简介
jpeg是JPEG图像压缩免费库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v9e
- 当前适配的功能：支持JPEG图像压缩
- [Libjpeg License](https://jpegclub.org/reference/libjpeg-license)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
