# double-conversion三方库说明
## 功能简介
double-conversion用于IEEE高效二进制-十进制和十进制-二进制转换。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v3.2.1
- 当前适配的功能：IEEE高效二进制-十进制和十进制-二进制转换
- [BSD](https://github.com/google/double-conversion)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
