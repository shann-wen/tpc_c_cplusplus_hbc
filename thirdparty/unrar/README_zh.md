# unrar三方库说明
## 功能简介
unrar是一个解压rar文件的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta1版本
- OpenHarmony SDK版本：API version 9版本
- 三方库版本：v1.0.0
- 当前适配的功能：支持解压rar格式的文件
- [Apache License 2.0](https://github.com/maoabc/unrar-android/blob/master/LICENSE)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
