# json三方库说明
## 功能简介
json是一个C++的处理json数据解析的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v3.11.2
- 当前适配的功能：json数据解析
- [MIT](https://github.com/nlohmann/json)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
