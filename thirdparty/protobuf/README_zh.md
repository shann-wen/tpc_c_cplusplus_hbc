# protobuf三方库说明
## 功能简介
protobuf是Google提供的一套数据的序列化框架。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v4.23.2
- 当前适配的功能：提供数据的序列化框架
- [BSD-3-Clause license](https://github.com/protocolbuffers/protobuf/blob/main/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)