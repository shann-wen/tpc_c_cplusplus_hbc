# libass三方库说明
## 功能简介
libass库则是一个轻量级的对ASS/SSA格式字幕进行渲染的开源库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：0.17.1
- 当前适配的功能：支持ASS/SSA格式字幕进行渲染
- [ISC License](https://github.com/libass/libass/blob/master/COPYING)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
