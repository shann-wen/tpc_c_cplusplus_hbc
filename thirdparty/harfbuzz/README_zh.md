# harfbuzz三方库说明
## 功能简介
harfbuzz是一个有OpenType文本整形能力的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：7.1.0
- 当前适配的功能：支持OpenType文本整形能力
- [Old MIT](https://github.com/harfbuzz/harfbuzz/blob/main/COPYING)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
