# diff-match-patch-cpp-stl三方库说明
## 功能简介
C++ STL variant of https://code.google.com/p/google-diff-match-patch.
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：master
- 当前适配的功能：支持文本对比等功能。
- [Apache License 2.0](https://github.com/leutloff/diff-match-patch-cpp-stl/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
