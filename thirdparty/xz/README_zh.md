# xz 三方库说明

## 功能简介

XZ 是免费的通用数据压缩软件，具有较高的压缩比。

## 使用约束

- ROM版本：OpenHarmony3.2 Beta1
- 三方库版本：5.2.6
- 当前适配的功能：完成了 .lzma 格式文件的压缩、解压缩
- [License : GPLv2](https://git.tukaani.org/?p=xz.git;a=blob;f=COPYING.GPLv2;h=d159169d1050894d3ea3b98e1c965c4058208fe1;hb=8dfed05bdaa4873833ba24279f02ad2db25effea)

## 集成方式

- [系统Rom包集成](docs/rom_integrate.md)
- [应用hap包集成](docs/hap_integrate.md)
