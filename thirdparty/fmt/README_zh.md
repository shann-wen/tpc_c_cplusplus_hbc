# fmt 三方库说明
## 功能简介
fmt是一个开源格式库，可提供C stdio和C ++ iostreams的快速安全替代品。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 4.0.3.6 (API Version 9 Release)
- 三方库版本：10.0.0
- 当前适配的功能：提供C stdio和C ++ iostreams的快速安全替代品
- [GNU Library General Public License](https://github.com/fmtlib/fmt/blob/master/LICENSE.rst)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
