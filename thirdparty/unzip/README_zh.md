# unzip三方库说明
## 功能简介
unzip为zip压缩文件的解压缩程序。
## 使用约束
- IDE版本：DevEco Studio 3.1 Release(3.1.0.500)
- SDK：API9 Release(3.2.12.2)
- 三方库版本：60
- 当前适配的功能：解压缩zip文件
- [Info-ZIP license](https://sourceforge.net/projects/infozip/)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
