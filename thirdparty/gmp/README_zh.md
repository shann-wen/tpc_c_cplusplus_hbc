# gmp三方库说明
## 功能简介
gmp是用于任意精度算术的运算库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：v1.0.12
- 当前适配的功能：支持任意精度算术
- [GNU LGPL v3 | GNU GPL v2](https://gmplib.org/)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
