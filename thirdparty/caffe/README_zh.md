# caffe三方库说明
## 功能简介
Caffe：一个用于深度学习的快速开放框架。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：1.0
- 当前适配的功能：支持深度学习
- [Copyright (c) 2014-2017 The Regents of the University of California](https://github.com/BVLC/caffe/blob/master/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
