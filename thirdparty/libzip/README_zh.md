# libzip三方库说明
## 功能简介
libzip是一个用于读取、创建和修改zip存档的C库。文件可以从数据缓冲区、文件或直接从其他zip档案复制的压缩数据中添加。

## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：1.9.2
- 当前适配的功能：读取、创建和修改zip存档功能
- [BSD License](https://github.com/nih-at/libzip/blob/main/LICENSE)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
