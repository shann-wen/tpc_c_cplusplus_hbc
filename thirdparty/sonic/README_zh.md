# sonic三方库说明
## 功能简介
sonic是一种用于加速或减慢音频算法的库。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：release-0.2.0
- 当前适配的功能：支持音频变速
- [Apache License 2.0](https://github.com/waywardgeek/sonic)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
