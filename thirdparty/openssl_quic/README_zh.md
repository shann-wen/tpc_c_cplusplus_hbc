# openssl_quic三方库说明
## 功能简介
openssl_quic是openssl加密库的一个分支用于启用quic。
## 使用约束
- IDE版本：DevEco Studio 3.1 Beta2
- SDK版本：ohos_sdk_public 3.2.11.9 (API Version 9 Release)
- 三方库版本：OpenSSL_1_1_1t-quic1
- 当前适配的功能：支持启动openssl quic
- [Apache License 2.0](https://github.com/quictls/openssl)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
