# 三方库列表

| 三方库名称                                                   | 开源协议                                      | 南向 | 北向 |
| ------------------------------------------------------------ | --------------------------------------------- | ---- | ---- |
| [xerces-c](../thirdparty/xerces-c/README_zh.md)              | Apache License 2.0                            | 是   | 是   |
| [modbus](../thirdparty/modbus/README_zh.md)                  | LGPL v2.1 or Later                            | 是   |      |
| [tinyxpath](../thirdparty/tinyxpath/README_zh.md)            | zlib/libpng License                           |      | 是   |
| [bsdiff](../thirdparty/bsdiff/README_zh.md)                  | BSD 2-clause license                          | 是   |      |
| [concurrentqueue](../thirdparty/concurrentqueue/README_zh.md) | BSD License                                   | 是   | 是   |
| [iconv](../thirdparty/iconv/README_zh.md)                    | LGPLv3.0                                      | 是   |      |
| [jbig2enc](../thirdparty/jbig2enc/README_zh.md)              | Apache License                                | 是   |      |
| [leptonica](../thirdparty/leptonica/README_zh.md)            | leptonica-license                             | 是   |      |
| [libmp3lame](../thirdparty/libmp3lame/README_zh.md)          | LGPL                                          | 是   |      |
| [lzma](../thirdparty/lzma/README_zh.md)                      | GNU LGPLv2+                                   | 是   |      |
| [minizip-ng](../thirdparty/minizip-ng/README_zh.md)          | zlib License                                  | 是   | 是   |
| [mqtt](../thirdparty/mqtt/README_zh.md)                      | Eclipse Public License v2.0                   | 是   |      |
| [openjpeg](../thirdparty/openjpeg/README_zh.md)              | 2-clauses BSD license                         | 是   |      |
| [rapidjson](../thirdparty/rapidjson/README_zh.md)            | MIT license                                   | 是   |      |
| [tinyxml2](../thirdparty/tinyxml2/README_zh.md)              | zlib license                                  | 是   | 是   |
| [unrar](../thirdparty/unrar/README_zh.md)                    | Apache License 2.0                            |      | 是   |
| [xz](../thirdparty/xz/README_zh.md)                          | GPLv2                                         | 是   |      |
| [zstd](../thirdparty/zstd/README_zh.md)                      | BSD and GPLv2                                 | 是   |      |
| [busybox](../thirdparty/busybox/README_zh.md)                | GPL 2.0                                       | 是   |      |
| [cryptopp](../thirdparty/cryptopp/README_zh.md)              | Boost Software License and CRYPTOGAMS License |      | 是   |
| [double-conversion](../thirdparty/double-conversion/README_zh.md) | BSD                                           |      | 是   |
| [hunspell](../thirdparty/hunspell/README_zh.md)              | LGPL-2.1/GPL-2.0 License                      |      | 是   |
| [jasper](../thirdparty/jasper/README_zh.md)                  | JasPer License Version 2.0                    |      | 是   |
| [json-schema-validator](../thirdparty/json-schema-validator/README_zh.md) | MIT License                                   |      | 是   |
| [libtommath](../thirdparty/libtommath/README_zh.md)          | The LibTom license                            |      | 是   |
| [libxlsxwriter](../thirdparty/libxlsxwriter/README_zh.md)    | FreeBSD license                               |      | 是   |
| [nghttp3](../thirdparty/nghttp3/README_zh.md)                | MIT License                                   |      | 是   |
| [phf](../thirdparty/phf/README_zh.md)                        | MIT License                                   |      | 是   |
| [pugixml](../thirdparty/pugixml/README_zh.md)                | MIT License                                   |      | 是   |
| [tinyexr](../thirdparty/tinyexr/README_zh.md)                | 3-clause BSD                                  |      | 是   |
| [WavPack](../thirdparty/WavPack/README_zh.md)                | BSD 3-Clause "New" or "Revised" License       |      | 是   |
| [libsrtp](../thirdparty/libsrtp/README_zh.md)                | Copyright (c) 2001-2017 Cisco Systems         |      | 是   |
| [log4cplus](../thirdparty/log4cplus/README_zh.md)            | BSD 2.0/Apache 2.0                            |      | 是   |
| [zxing-cpp](../thirdparty/zxing-cpp/README_zh.md)            | Apache License 2.0                            |      | 是   |
| [libsvm](../thirdparty/libsvm/README_zh.md)                  | BSD                                           |      | 是   |
| [geos](../thirdparty/geos/README_zh.md)                      | LGPL v2.1                                     |      | 是   |
| [jbig2dec](../thirdparty/jbig2dec/README_zh.md)              | GPLv3                                         |      | 是   |
| [faad2](../thirdparty/faad2/README_zh.md)                    | GPLv2                                         |      | 是   |
| [libxls](../thirdparty/libxls/README_zh.md)                  | BSD                                           |      | 是   |
| [libtess2](../thirdparty/libtess2/README_zh.md)              | SGI FREE SOFTWARE LICENSE B                   |      | 是   |
| [djvulibre](../thirdparty/djvulibre/README_zh.md)            | GPLv2                                         |      | 是   |
| [expat](../thirdparty/libexpat/README_zh.md)                 | MIT License                                   |      | 是   |
| [exiv2](../thirdparty/exiv2/README_zh.md)                    | GPL 2.0                                       |      | 是   |
| [libpng](../thirdparty/libpng/README_zh.md)                  | zlib/libpng License                           |      | 是   |
| [soundtouch](../thirdparty/soundtouch/README_zh.md)          | LGPLv2.1                                      |      | 是   |
| [jbigkit](../thirdparty/jbigkit/README_zh.md)                | GNU General Public License                    |      | 是   |
| [zbar](../thirdparty/zbar/README_zh.md)                      | LGPL2.1                                       |      | 是   |
| [marisa-trie](../thirdparty/marisa-trie/README_zh.md)        | LGPL-2.1/GPL-2.0                              |      | 是   |
| [fribidi](../thirdparty/fribidi/README_zh.md)                | LGPL-2.1 license                              |      | 是   |
| [cJSON](../thirdparty/cJSON/README_zh.md)                    | MIT license                                   |      | 是   |
| [libqrencode](../thirdparty/libqrencode/README_zh.md)        | MIT license                                   |      | 是   |
| [pupnp](../thirdparty/pupnp/README_zh.md)                    | LGPL-2.1 license                              |      | 是   |
| [sonic](../thirdparty/sonic/README_zh.md)                    | Apache License 2.0                            |      | 是   |
| [libzip](../thirdparty/libzip/README_zh.md)                  | BSD License                                   |      | 是   |
| [harfbuzz](../thirdparty/harfbuzz/README_zh.md)              | Old MIT                                       |      | 是   |
| [googletest](../thirdparty/googletest/README_zh.md)          | BSD-3-Clause license                          |      | 是   |
| [jsoncpp](../thirdparty/jsoncpp/README_zh.md)                | MIT License                                   |      | 是   |
| [kissfft](../thirdparty/kissfft/README_zh.md)                | BSD-3-Clause                                  |      | 是   |
| [gmp](../thirdparty/gmp/README_zh.md)                        | GNU LGPL v3 \| GNU GPL v2                     |      | 是   |
| [jpeg](../thirdparty/jpeg/README_zh.md)                      | Libjpeg License                               |      | 是   |
| [miniini](../thirdparty/miniini/README_zh.md)                | MIT License                                   |      | 是   |
| [pixman](../thirdparty/pixman/README_zh.md)                  | MIT License                                   |      | 是   |
| [libheif](../thirdparty/libheif/README_zh.md)                | GNU LESSER GENERAL PUBLIC LICENSE             |      | 是   |
| [libxml2](../thirdparty/libxml2/README_zh.md)                | MIT License                                   |      | 是   |
| [sqlite](../thirdparty/sqlite/README_zh.md)                  | Public Domain                                 |      | 是   |
| [libuuid](../thirdparty/libuuid/README_zh.md)                | BSD                                           |      | 是   |
| [giflib](../thirdparty/giflib/README_zh.md)                  | giflib COPYING                                |      | 是   |
| [lcms2](../thirdparty/lcms2/README_zh.md)                    | MIT License                                   |      | 是   |
| [websocketpp](../thirdparty/websocketpp/README_zh.md)        | websocketpp license                           |      | 是   |
| [p7zip](../thirdparty/p7zip/README_zh.md)                    | LGPL                                          |      | 是   |
| [freetype2](../thirdparty/freetype2/README_zh.md)            | eneral Public License]                        |      | 是   |
| [curl](../thirdparty/curl/README_zh.md)                      | MIT License                                   |      | 是   |
| [openssl](../thirdparty/openssl/README_zh.md)                | Apache License 2.0                            |      | 是   |
| [libdash](../thirdparty/libdash/README_zh.md)                | LGPLv2.1                                      |      | 是   |
| [mythes](../thirdparty/mythes/README_zh.md)                  | BSD                                           |      | 是   |
| [libass](../thirdparty/libass/README_zh.md)                  | ISC License                                   |      | 是   |
| [diff-match-patch-cpp-stl](../thirdparty/diff-match-patch-cpp-stl/README_zh.md) | Apache License 2.0                            |      | 是   |
| [uchardet](../thirdparty/uchardet/README_zh.md)              | OZILLA PUBLIC LICENSE/PLV2/GPLV2              |      | 是   |
| [unzip](../thirdparty/unzip/README_zh.md)                    | Info-ZIP license                              |      | 是   |
| [xmlrpc-c](../thirdparty/xmlrpc-c/README_zh.md)              | BSD license                                   |      | 是   |
| [protobuf](../thirdparty/protobuf/README_zh.md)              | BSD-3-Clause license                          |      | 是   |
| [unixODBC](../thirdparty/unixODBC/README_zh.md)              | GNU Lesser General Public License v2.1        |      | 是   |
| [fmt](../thirdparty/fmt/README_zh.md)                        | GNU Lesser General Public License             |      | 是   |
| [assimp](../thirdparty/assimp/README_zh.md)                  | 3-clause BSD                                  |      | 是   |
| [thrift](../thirdparty/thrift/README_zh.md)                  | Apache License 2.0                            |      | 是   |
| [caffe](../thirdparty/caffe/README_zh.md)                    | caffe license                                 |      | 是   |

